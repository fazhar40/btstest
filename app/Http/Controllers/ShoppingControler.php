<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Shop;
use Validator;


class ShoppingControler extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function Create(Request $request)
    {
 

            $shop = new Shop;
            $shop->name = $request->input('name');

            $shop->save();

            //return successful response
            return response()->json(['shop' => $shop, 'message' => 'CREATED'], 201);
            

            

    }


    
    

}